package game;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class Menu extends JFrame{
	
	public static void main(String[] args) {
        
        EventQueue.invokeLater(() -> {
            JFrame ex = new SnakeGame();
            
            ex.setVisible(true);
        });
    }

}
