package game;
import javax.swing.Timer;

public class Snake {
	
	private int dots;
	private int DOT_SIZE = 10;
	protected int x[] = new int[3600];
	protected int y[] = new int[3600];
	protected int points;
	
	private boolean leftDirection = false;
    private boolean rightDirection = true;
    private boolean upDirection = false;
    private boolean downDirection = false;
    private boolean inGame = true;
    
    public Snake() 
    {
    	dots = 3;
    	points = 0;
    }
    
	public int getDots() {
		return dots;
	}

	public void setDots(int dots) {
		this.dots = dots;
	}

	
	public int getDOT_SIZE() {
		return DOT_SIZE;
	}

	public void setDOT_SIZE(int dOT_SIZE) {
		DOT_SIZE = dOT_SIZE;
	}

	
	public int[] getX() {
		return x;
	}

	public void setX(int[] x) {
		this.x = x;
	}

	
	public int[] getY() {
		return y;
	}

	public void setY(int[] y) {
		this.y = y;
	}

	
	public int getPoints(){
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}
	
	public boolean isLeftDirection() {
		return leftDirection;
	}

	public void setLeftDirection(boolean leftDirection) {
		this.leftDirection = leftDirection;
	}

	
	public boolean isRightDirection() {
		return rightDirection;
	}

	public void setRightDirection(boolean rightDirection) {
		this.rightDirection = rightDirection;
	}

	
	public boolean isUpDirection() {
		return upDirection;
	}

	public void setUpDirection(boolean upDirection) {
		this.upDirection = upDirection;
	}

	
	public boolean isDownDirection() {
		return downDirection;
	}

	public void setDownDirection(boolean downDirection) {
		this.downDirection = downDirection;
	}

	
	public boolean isInGame() {
		return inGame;
	}

	public void setInGame(boolean inGame) {
		this.inGame = inGame;
	}
	
	public void move(){
        for (int z = dots; z > 0; z--) {
            x[z] = x[(z - 1)];
            y[z] = y[(z - 1)];
        }
        if (leftDirection) {
            x[0] -= DOT_SIZE;
        }
        if (rightDirection) {
            x[0] += DOT_SIZE;
        }
        if (upDirection) {
            y[0] -= DOT_SIZE;
        }
        if (downDirection) {
            y[0] += DOT_SIZE;
        }
    }
	
	public void checkCollision(Timer timer) {	
        for (int z = dots; z > 0; z--) {
            if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                inGame = false;
            }
        }
        if (y[0] >= 590) {
            inGame = false;
        }
        if (y[0] <= 0) {
            inGame = false;
        }
        if (x[0] >= 590) {
            inGame = false;
        }
        if (x[0] <= 0) {
            inGame = false;
        }
        if (y[0] == 200 && x[0] >= 200 && x[0] <= 380) 
        {
        	inGame = false;
        }
        if (x[0] == 200 && y[0] >= 200 && y[0] <= 380) 
        {
        	inGame = false;
        }
        if (y[0] == 380 && x[0] >= 200 && x[0] <= 380) 
        {	
        	if(x[0] <= 237 || x[0] >= 350){
        		inGame = false;
        		}
        		
        }
        if (x[0] == 380 && y[0] >= 200 && y[0] <= 380) 
        {	
        		inGame = false;
        }

        if(!inGame) {
            timer.stop();
        }
    }
}
