package game;

public class GerarFruit extends Thread{
	
	private int RAND_POS = 59;
	private int RAND_FRUIT;
	Fruit simpleFruit;
	BombFruit bombFruit;
	BigFruit bigFruit;
	DecreaseFruit decFruit;
	
	
	public GerarFruit(Fruit fruit,BombFruit bomb,BigFruit big,DecreaseFruit dec) 
	{	
		simpleFruit = fruit;
		bombFruit = bomb;
		bigFruit = big;
		decFruit = dec;
		RAND_FRUIT = 0;
	}
	
	public int getRAND_FRUIT() {
		return RAND_FRUIT;
	}

	public void setRAND_FRUIT(int rAND_FRUIT) {
		RAND_FRUIT = rAND_FRUIT;
	}
	
	public void resetFruit() 
	{
		while(true){
			RAND_FRUIT = (int) (Math.random() * 4);
		
			if(RAND_FRUIT == 0) {
				do {
					
		        	int r = (int) (Math.random() * RAND_POS);
		        	simpleFruit.setFruit_x(((r * 10)));
		        	
				}while(simpleFruit.getFruit_x() == 0 || simpleFruit.getFruit_x() >= 580);
				
		        do{
		        	
		        	do{
		        		
		        		int r = (int) (Math.random() * RAND_POS);
		        		simpleFruit.setFruit_y(((r * 10)));
		            	
		        	}while(
		        	simpleFruit.getFruit_x() == 200 && simpleFruit.getFruit_y() <= 380 && simpleFruit.getFruit_y() >= 200);
		    		
		        }while(simpleFruit.getFruit_y() == 0 || simpleFruit.getFruit_y() >= 580);
			}
			
			else if(RAND_FRUIT == 1) {
				do {
					
		        	int r = (int) (Math.random() * RAND_POS);
		        	bigFruit.setFruit_x(((r * 10)));
		        	
				}while(bigFruit.getFruit_x() == 0 || bigFruit.getFruit_x() >= 580);
				
		        do{
		        	
		        	do{
		        		
		        		int r = (int) (Math.random() * RAND_POS);
		        		bigFruit.setFruit_y(((r * 10)));
		            	
		        	}while(
		        			bigFruit.getFruit_x() == 200 && bigFruit.getFruit_y() <= 380 && bombFruit.getFruit_y() >= 200);
		    		
		        }while(bigFruit.getFruit_y() == 0 || bigFruit.getFruit_y() >= 580);
			}
			
			else if(RAND_FRUIT == 2) {
				do {
					
		        	int r = (int) (Math.random() * RAND_POS);
		        	bombFruit.setFruit_x(((r * 10)));
		        	
				}while(bombFruit.getFruit_x() == 0 || bombFruit.getFruit_x() >= 580);
				
		        do{
		        	
		        	do{
		        		
		        		int r = (int) (Math.random() * RAND_POS);
		        		bombFruit.setFruit_y(((r * 10)));
		            	
		        	}while(
		        	bombFruit.getFruit_x() == 200 && bombFruit.getFruit_y() <= 380 && bigFruit.getFruit_y() >= 200);
		    		
		        }while(bombFruit.getFruit_y() == 0 || bombFruit.getFruit_y() >= 580);
			}
			else if(RAND_FRUIT == 3) {
				do {
					
		        	int r = (int) (Math.random() * RAND_POS);
		        	decFruit.setFruit_x(((r * 10)));
		        	
				}while(decFruit.getFruit_x() == 0 || decFruit.getFruit_x() >= 580);
				
		        do{
		        	
		        	do{
		        		
		        		int r = (int) (Math.random() * RAND_POS);
		        		decFruit.setFruit_y(((r * 10)));
		            	
		        	}while(
		        	decFruit.getFruit_x() == 200 && decFruit.getFruit_y() <= 380 && decFruit.getFruit_y() >= 200);
		    		
		        }while(decFruit.getFruit_y() == 0 || decFruit.getFruit_y() >= 580);
			}
			try {
				sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void run() 
	{	
		resetFruit();
	}
}
	

