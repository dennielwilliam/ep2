package game;

public class BigFruit extends Fruit{
	
	@Override
	public void checkFruit(Snake snake) {
		int x[] = snake.getX();
		int y[] = snake.getY();
		
		if ((x[0] == getFruit_x()) && (y[0] == getFruit_y())) {
        	snake.setDots(snake.getDots() + 1);
            snake.setPoints(snake.getPoints() + 2);
            getGerarFruit().interrupt();
        }
	}
	@Override
	public void checkFruit(SnakeKitty snake) {
		int x[] = snake.getX();
		int y[] = snake.getY();
		
		if ((x[0] == getFruit_x()) && (y[0] == getFruit_y())) {
        	snake.setDots(snake.getDots() + 1);
            snake.setPoints(snake.getPoints() + 2);
            getGerarFruit().interrupt();
        }
		
	}
	@Override
	public void checkFruit(SnakeStar snake) {
		int x[] = snake.getX();
		int y[] = snake.getY();
		
		if ((x[0] == getFruit_x()) && (y[0] == getFruit_y())) {
        	snake.setDots(snake.getDots() + 1);
            snake.setPoints(snake.getPoints() + 2);
            getGerarFruit().interrupt();
        }
	}
}
