package game;

public class Fruit {
	
	private int fruit_x;
	private int fruit_y;
	GerarFruit gerarFruit;
	
	public int getFruit_y() {
		return fruit_y;
	}
	public void setFruit_y(int fruit_y) {
		this.fruit_y = fruit_y;
	}

	public int getFruit_x() {
		return fruit_x;
	}
	public void setFruit_x(int fruit_x) {
		this.fruit_x = fruit_x;
	}
	
	public GerarFruit getGerarFruit() {
		return gerarFruit;
	}
	public void setGerarFruit(GerarFruit gerarFruit) {
		this.gerarFruit = gerarFruit;
	}
	
	public void checkFruit(Snake snake) {
		int x[] = snake.getX();
		int y[] = snake.getY();
			
		if ((x[0] == fruit_x) && (y[0] == fruit_y)) {
            snake.setDots(snake.getDots() + 1);
            snake.setPoints(snake.getPoints() + 1);
            gerarFruit.interrupt();
        }
    }
	public void checkFruit(SnakeKitty snake) {
		int x[] = snake.getX();
		int y[] = snake.getY();
			
		if ((x[0] == fruit_x) && (y[0] == fruit_y)) {
            snake.setDots(snake.getDots() + 1);
            snake.setPoints(snake.getPoints() + 1);
            gerarFruit.interrupt();
        }
    }
	public void checkFruit(SnakeStar snake) {
		int x[] = snake.getX();
		int y[] = snake.getY();
			
		if ((x[0] == fruit_x) && (y[0] == fruit_y)) {
            snake.setDots(snake.getDots() + 1);
            snake.setPoints(snake.getPoints() + 1);
            gerarFruit.interrupt();
        } 
    }
}
