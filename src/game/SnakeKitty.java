package game;

import javax.swing.Timer;

public class SnakeKitty extends Snake{
	
	@Override
	public void checkCollision(Timer timer) {	
        for (int z = getDots(); z > 0; z--) {
            if ((z > 4) && (x[0] == x[z]) && (y[0] == y[z])) {
                setInGame(false);
            }
        }
        if (y[0] >= 590) {
        	setInGame(false);
        }
        if (y[0] <= 0) {
        	setInGame(false);
        }
        if (x[0] >= 590) {
        	setInGame(false);
        }
        if (x[0] <= 0) {
        	setInGame(false);
        }
      
        if(!isInGame()) {
            timer.stop();
        }
    }
}
