package game;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class SnakeGame extends JFrame {
	
    public SnakeGame() {
        
        initUI();
    }
    
    private void initUI() {
        int choose = (int) (Math.random() * 3);
        
    	//add(new Score());
    	
    	if(choose == 0) {
    		add(new Board());
    		System.out.println("Foi escolhido Simple");
    	}
    	else if(choose == 1) 
    	{
    		add(new BoardKitty());
    		System.out.println("Foi escolhido Kitty");
    	}
    	else if(choose == 2) 
    	{
    		add(new BoardStar());
    		System.out.println("Foi escolhido Star");
    	}
               
        setResizable(false);
        pack();
        
        setTitle("Snake");
        setLocationRelativeTo(null);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
             
    }
    
}