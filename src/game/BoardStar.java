package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class BoardStar extends JPanel implements ActionListener {

    private final int B_WIDTH = 600;
    private final int B_HEIGHT = 600;
    private final int DELAY = 140;
    private Timer timer;
    private Image ball;
    private Image Fruit;
    private Image DecreaseFruit;
    private Image BombFruit;
    private Image BigFruit;
    private Image Walls;
    private Image head;
    SnakeStar snake = new SnakeStar();
    Fruit fruit = new Fruit();
	BombFruit bombFruit = new BombFruit();
	BigFruit bigFruit = new BigFruit();
	DecreaseFruit decFruit = new DecreaseFruit();
	GerarFruit gerarFruit = new GerarFruit(fruit,bombFruit,bigFruit,decFruit);
	
	
	
    public BoardStar() {
    	
    	fruit.setGerarFruit(gerarFruit);
    	bombFruit.setGerarFruit(gerarFruit);
    	bigFruit.setGerarFruit(gerarFruit);
    	decFruit.setGerarFruit(gerarFruit);
    	
    	
        initBoard();
    }
      
    private void initBoard() {

        addKeyListener(new TAdapter());
        setBackground(Color.black);
        setFocusable(true);

        setPreferredSize(new Dimension(B_WIDTH, B_HEIGHT));
        loadImages();
        initGame();
        
    }

    private void loadImages() {

        ImageIcon iid = new ImageIcon("src/resources/dot.png");
        ball = iid.getImage();
        
        ImageIcon iih = new ImageIcon("src/resources/head.png");
        head = iih.getImage();
        
        ImageIcon iiw = new ImageIcon("src/resources/walls.png");
        Walls = iiw.getImage();
        
        ImageIcon iif = new ImageIcon("src/resources/Fruit.png");
        Fruit = iif.getImage();
        
        ImageIcon iibig = new ImageIcon("src/resources/BigFruit.png");
        BigFruit = iibig.getImage();
        
        ImageIcon iibomb = new ImageIcon("src/resources/BombFruit.png");
        BombFruit = iibomb.getImage();
        
        ImageIcon iiDec = new ImageIcon("src/resources/DecreaseFruit.png");
        DecreaseFruit = iiDec.getImage();
     
    }

    private void initGame() {
 
    	int x[] = snake.getX();
    	int y[] = snake.getY();
    	
    	
    	for (int z = 0; z < snake.getDots(); z++) {
    		x[z] = 50 - z * 10;
    		y[z] = 50;
    	}
    	
        timer = new Timer(DELAY, this);
        timer.start();
        gerarFruit.start();
        snake.setX(x);
        snake.setY(y);
   
        
    }
    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        doBackground(g);
        doDrawing(g);
        
    }
    private void doBackground(Graphics g) 
    {
    	int columns, lines;
        
        for(lines = 0;lines < 591;lines++) {
        	g.drawImage(Walls,lines,0,this);
        }
        
        for(columns = 0;columns < 591;columns++) {
        	g.drawImage(Walls,0,columns,this);
        }
        
       
        for(lines = 600;lines > 0;lines--) {
        	g.drawImage(Walls,lines,591,this);
        }
        
        for(columns = 600;columns > 0;columns--) {
        	g.drawImage(Walls,591,columns,this);
        }
        
        
        for(lines = 200;lines < 380;lines++) {
        	g.drawImage(Walls,lines,200,this);
        }
        
        for(columns = 200;columns < 380;columns++) {
        	g.drawImage(Walls,200,columns,this);
        }
        
        for(columns = 380;columns > 200;columns--) {
        	g.drawImage(Walls,380,columns,this);
        }
        
        for(lines = 380;lines > 200;lines--) {
        	g.drawImage(Walls,lines,380,this);
        	
        	if(lines == 350) {
        		lines = 237;
        	}
        }
                    
           
    }
    private void doDrawing(Graphics g) {
        int x[] = snake.getX();
        int y[] = snake.getY();
        
        if (snake.isInGame()){
        	
        	if(gerarFruit.getRAND_FRUIT() == 0) {
            	g.drawImage(Fruit, fruit.getFruit_x(), fruit.getFruit_y(), this);
        	}
        	else if(gerarFruit.getRAND_FRUIT() == 1) {
        		g.drawImage(BigFruit, bigFruit.getFruit_x(), bigFruit.getFruit_y(), this);
        	}
        	else if(gerarFruit.getRAND_FRUIT() == 2) {
	            g.drawImage(BombFruit, bombFruit.getFruit_x(), bombFruit.getFruit_y(), this);
        	}
        	else if(gerarFruit.getRAND_FRUIT() == 3) {
	            g.drawImage(DecreaseFruit, decFruit.getFruit_x(), decFruit.getFruit_y(), this);
        	}

            for (int z = 0; z < snake.getDots(); z++) {
                if (z == 0) {
                    g.drawImage(head, x[z], y[z], this);
                } else {
                    g.drawImage(ball, x[z], y[z], this);
                }
            }

            Toolkit.getDefaultToolkit().sync();

        } else {
            gameOver(g);
        }        
    }

    private void gameOver(Graphics g) {
        String msg = "Game Over";
        Font small = new Font("Helvetica", Font.BOLD, 14);
        FontMetrics metr = getFontMetrics(small);
        g.setColor(Color.white);
        g.setFont(small);
        g.drawString(msg, (B_WIDTH - metr.stringWidth(msg)) / 2, B_HEIGHT / 2);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {

        if (snake.isInGame()){
            fruit.checkFruit(snake);
            bombFruit.checkFruit(snake);
            bigFruit.checkFruit(snake);
            decFruit.checkFruit(snake);
            snake.checkCollision(timer);
            snake.move();
        }
        repaint();
    }

    private class TAdapter extends KeyAdapter {

        @Override
        public void keyPressed(KeyEvent e) {

            int key = e.getKeyCode();

            if ((key == KeyEvent.VK_LEFT) && (!snake.isRightDirection())) {
                snake.setLeftDirection(true);
                snake.setUpDirection(false);
                snake.setDownDirection(false);
            }

            if ((key == KeyEvent.VK_RIGHT) && (!snake.isLeftDirection())) {
            	snake.setRightDirection(true);
                snake.setUpDirection(false);
                snake.setDownDirection(false);
            }

            if ((key == KeyEvent.VK_UP) && (!snake.isDownDirection())) {
                snake.setUpDirection(true);
                snake.setRightDirection(false);
                snake.setLeftDirection(false);
            }

            if ((key == KeyEvent.VK_DOWN) && (!snake.isUpDirection())) {
            	snake.setDownDirection(true);
                snake.setRightDirection(false);
                snake.setLeftDirection(false);
            }
        }
    }
}